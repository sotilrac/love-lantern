// web_creds.h
// General purpose test functions

#ifndef WEB_CREDS_H
#define WEB_CREDS_H

struct Creds {
    char host_address[48];
    char wifi_ssid[48];
    char wifi_password[48];
    char client_id[48];
    char topic_pub[48];
    char topic_sub[48];
};

Creds web = {
    "[aws iot host]",
    # ifdef DEVICE_A
    "[your wifi SSID]",
    "[your wifi password]",
    "lanter_a",
    "$aws/[path to your thing]/touch/a",
    "$aws/[path to your thing]/touch/b",
    # else
    "[your wifi SSID]",
    "[your wifi password]",
    "lantern_b",
    "$aws/[path to your thing]/touch/b",
    "$aws/[path to your thing]/touch/a",
    # endif
};

#endif // WEB_CREDS_H
