# Love Lantern

![Love Lantern](assets/love_lantern.jpg "Love Lantern")

Long-distance relationships can be difficult. This pair of IoT lanterns that react to touch is intended to shorten the distance by providing a physical way to communicate. The operation is simple, when you think of your loved one, touch your lantern, and theirs will shine bright.

It turns out this can make a great gift, and building them results in a great bonding experience.

The lanterns use an ESP32 module to communicate with the AWS IoT service to exchange information.  

## Materials
* [A lovely lantern](https://smile.amazon.com/gp/product/B01J62PSVI)
* [A Huzzaa32 Feather](https://www.adafruit.com/product/3405)
* [A 2N7000 FET](https://smile.amazon.com/gp/product/B07BKX255D)
* [A 100 Ohm Resistor](https://smile.amazon.com/gp/product/B07BKVNBH6)
* [LiPo battery](https://smile.amazon.com/gp/product/B07BTTQDJQ)
* [Prototyping board](https://smile.amazon.com/gp/product/B074X2GDH2)
* [Wire](https://smile.amazon.com/gp/product/B01LH1G2IE)

## Tools
* Soldering iron
* Wire stripper

## Circuit
Using a prototyping board, create the circuit below. Connect the touch sensor to the chassis of the lamp.
![Love Lantern Circuit](assets/love_lantern_schematics.png "Love Lantern Circuit")


## Software Dependencies

Use [Arduino IDE](https://www.arduino.cc/en/Main/Software) to compile the main program once the dependencies are installed. 

Install [AWS IoT dependencies](https://exploreembedded.com/wiki/AWS_IOT_with_Arduino_ESP32).

Create an AWS account and [create an AWS IoT thing](https://exploreembedded.com/wiki/Secure_IOT_with_AWS_and_Hornbill_ESP32).

Update the web_creds.h file with your IoT Credentials.

## To Do
The lanterns are functional but there's room for many improvements.

* Implement WiFi reconnection
* Implement WiFi settings via app
* Implement OTA
* Improve communication reliability
* Use one device certificate per lantern
* Improve IoT device management (maybe use lambdas?)
* Create PCB for circuit
